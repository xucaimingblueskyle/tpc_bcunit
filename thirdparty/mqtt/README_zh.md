# mqtt 三方库说明

## 功能简介

MQTT 是用 C 语言编写的用于MQTT协议的Eclipse Paho C客户端库。

## 使用约束

- ROM版本：OpenHarmony3.2 beta4
- 三方库版本：v1.3.12
- 当前适配的功能：完成Eclipse Paho C客户端库编译的适配

## 集成方式

- [系统Rom包集成](./docs/rom_integrate.md)。
