# openjpeg 三方库说明

## 功能简介

OpenJPEG 是用 C 语言编写的开源 JPEG 2000 编解码器。

## 使用约束

- ROM版本：OpenHarmony3.2 beta1
- 三方库版本：v2.5.0
- 当前适配的功能：[JPEG 2000](https://jpeg.org/jpeg2000/) 图像编解码

## 集成方式

- [系统Rom包集成](./docs/rom_integrate.md)。
